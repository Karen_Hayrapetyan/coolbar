<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacebookBarSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facebook_bar_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('share')->default('true');
            $table->integer('duration')->default(0);
            $table->string('text_fb')->default('standard');
            $table->integer('site_id')->unsigned();
            $table->foreign('site_id')->references('id')->on('sites')->onDelete('cascade');
            $table->string('back_color')->default('#00AABB');
            $table->string('text_color')->default('#3c299d');
            $table->string('text_size')->default('12');
            $table->string('text')->default('Like Us!!!');
            $table->string('bar_position')->default('top');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facebook_bar_settings');
    }
}
