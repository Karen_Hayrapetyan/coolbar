<?php

use Illuminate\Database\Seeder;
use App\Model\Bar;
class BarsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        if(Bar::count()<count($this->source())){
            Bar::where('status','1')->delete();
            $data = $this->source();
            foreach ($data as $val){

                Bar::create([
                    'bar_name'=>$val
                ]);
            }
        };
    }

    public function source(){
        return [
            'Facebook',
            'Quiz',
            'Sale',
            'Order a Call'
        ];
    }
}
