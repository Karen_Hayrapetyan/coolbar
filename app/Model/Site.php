<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Site extends Model
{
    protected $fillable = [

        'user_id','full_url', 'site_host','user_timezone'
    ];
}
