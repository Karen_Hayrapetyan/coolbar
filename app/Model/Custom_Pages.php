<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Custom_Pages extends Model
{
    protected $fillable = [
         'site_id','bar_id','page'

    ];
}
