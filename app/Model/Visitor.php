<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Visitor extends Model
{
    protected $fillable = [

        'site_id','client_ip', 'visits_count','bar_id'
    ];
}
