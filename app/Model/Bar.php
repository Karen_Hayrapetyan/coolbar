<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Bar extends Model
{
    protected $fillable = [

        'bar_name','status'
    ];
}
