<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class FacebookBarSettings extends Model
{
    protected $fillable = [
        'user_id', 'site_id','bar_id','back_color',
        'text_color','text_size','text','bar_position','share','text_fb',
        'duration'

    ];
}
