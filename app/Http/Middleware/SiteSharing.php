<?php

namespace App\Http\Middleware;

use Closure;
use App\Model\Site;
use Illuminate\Support\Facades\Auth;

class SiteSharing
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        /*
         *
         * Sharing User created sites
         *
         * */
        $sites = Site::where('user_id',Auth::user()->id)->get();
        if($sites){
            view()->share('sites', $sites);
        }else{

            view()->share('sites', 'You Have Not Created SiteSharing Yet');
        }

        return $next($request);
    }
}
