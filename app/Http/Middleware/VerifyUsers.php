<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class VerifyUsers
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /*
         * Checking Is Verify User
         *
         * */
        if (Auth::guard('web')->getUser()->verifyToken != NULL) {

            return redirect()->route('not-valid');

            } else {
                return $next($request);
            }

    }

}