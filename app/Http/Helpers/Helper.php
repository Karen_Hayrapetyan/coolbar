<?php
 use Illuminate\Support\Facades\Session;

    function ClientIp(){

        // Get client's IP address
        if (isset($_SERVER['HTTP_CLIENT_IP']) && array_key_exists('HTTP_CLIENT_IP', $_SERVER)) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
            $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $ips = array_map('trim', $ips);
            $ip = $ips[0];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'] ?? '0.0.0.0';
        }

        $ip = filter_var($ip, FILTER_VALIDATE_IP);
        $ip = ($ip === false) ? '0.0.0.0' : $ip;

        return $ip;
    }



    function checkCustomUrl($pages,$bar_id,$action){

        if($action == 'update'){
            foreach($pages as $page) {

               if($page != '/'){
                   \App\Model\Custom_Pages::create([
                       'site_id'=>Auth::user()->default_site,
                       'page' => $page,
                       'bar_id'=> $bar_id
                   ]);
               }
           }
            Session::flash('msg','Your Configs  Edited.');
            return redirect('/home');

       }elseif ($action == 'create'){

           // add custom pages into DB
           foreach($pages as $page) {
               if($page != '/'){
                   \App\Model\Custom_Pages::create([
                       'site_id'=>Auth::user()->default_site,
                       'page' => $page,
                       'bar_id' => $bar_id,
                   ]);
               }
           }

           Session::flash('msg','Your Configs  saved.');
           return redirect('/home');
       }

    }