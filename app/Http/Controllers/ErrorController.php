<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ErrorController extends Controller
{
    /*
     * return not Valid page when not verified user legged in
     * */
    public function notValid(){
        return view('errors/not_valid');
    }
}
