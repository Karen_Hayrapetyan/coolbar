<?php

namespace App\Http\Controllers;

use App\Model\Site;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class UserSitesController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');

        //share default site for auth user
        $this->middleware('site.sharing');

        // check if user confirmed email
        $this->middleware('verify.user');

    }


    public function index(){

        return view('sites/mySites');
    }

    /*
     *
     * Creating New Site
     *
     * */
    public function create(){

        $timezoneList = \DateTimeZone::listIdentifiers();
        return view('sites/create')->with('timezoneList',$timezoneList);

    }

    /*
     *
     * Save Created Site
     *
     * */

    public function save(Request $request){

        //add http:// if not exist in Url
        if (stripos($request->user_url, "http://")===false  && stripos($request->user_url, "https://")===false) {
            $request->user_url = "http://" . $request->user_url ;
        }

        // expload url in path,host,scheme parts
        $full_url = $request->user_url;
        $url = parse_url($full_url);

        $request['site_host'] = $url['host'];

        //validating request

        $this->validate($request,[
            'site_host'  => 'unique:sites,site_host',
            'full_url'  => 'unique:sites,full_url',
            'user_url' => array(
                'regex:#[-a-zA-Z0-9@:%_\+.~\#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~\#?&//=]*)?#si'
            ),
        ]);


        $user_timeZone = $request->timezone;

        //insert into db

        $site = Site::create([
            'full_url'=>$full_url,
            'user_id' => Auth::user()->id,
            'site_host' =>$url['host'],
            'user_timezone' =>$user_timeZone,
        ]);

        //Checking If User Have Not Created  SiteSharing Yet, Set it Default Site

        if(Auth::user()->default_site == 0){
            User::where('id', Auth::user()->id)->update(['default_site'=> $site->id]);
        }


        Session::flash('msg','Your Url  added.');
        return redirect()->action('HomeController@index');

    }

    /*
     *
     * Edit Site
     *
     * */

    public function edit($id){

        $data['site'] = Site::where('id',$id)->where('user_id',Auth::user()->id)->first();

        if($data['site']){
            $data['timezoneList'] =  \DateTimeZone::listIdentifiers();
            return view('sites/edit',$data);
        }else{
            Session::flash('msg','You Have Not Created Sites Yet.');
            return redirect()->action('HomeController@index');

        }

    }

    /*
     *
     * Save Edited Site
     *
     * */

    public function update(Request $request,$id){

        //add http:// if not exist in Url
        if (stripos($request->full_url, "http://")===false  && stripos($request->full_url, "https://")===false) {
            $request->full_url = "http://" . $request->full_url ;
        }

        // expload url in path,host,scheme
        $url = parse_url($request->full_url);

        $request['site_host'] = $url['host'];

        $this->validate($request,[
            'site_host'  => 'unique:sites,site_host,'.$id,
            'full_url'  => 'unique:sites,full_url,'.$id,
            'user_url' => array(
                'regex:#[-a-zA-Z0-9@:%_\+.~\#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~\#?&//=]*)?#si'
            ),
        ]);

        Site::where('id',$id)->where('user_id',Auth::user()->id)->update(array(
            'site_host' => $request['site_host'],
            'full_url' => $request['full_url'],
            'user_timezone' => $request['user_timezone'],
        ));


        Session::flash('msg','Your Configs  Edited.');
        return redirect()->action('HomeController@index');

    }



    /*
     * Delete Site
     *
     * */
    public function delete($id){

        //Deleting Site By Id

        Site::where('id',$id)->where('user_id',Auth::user()->id)->delete();

        // Checking if deleted site is default
        $default = User::where('id',Auth::user()->id)->where('default_site',$id)->first();

        if($default == null){
            Session::flash('msg','Site deleted successfully.');
            return redirect()->back();

        }else{


            $user_sites = Site::where('user_id',Auth::user()->id)->first();

            //Checking If User Has Site After Delete,Set It Default Site
            if($user_sites != null){

                User::where('id',Auth::user()->id)->update(['default_site'=> $user_sites->id]);
                Session::flash('msg','Default Site Changed.');
                return redirect()->back();
            }else{
                User::where('id',Auth::user()->id)->update(['default_site'=> 0]);
                return redirect()->back();
            }

        }
    }






    /*
     *
     * Changing default site By Ajax
     *
     * */
    public function change_default($id){

        $auth_user = Site::where('id',$id)->where('user_id',Auth::user()->id)->first();

        if($auth_user){

            User::where('id', Auth::user()->id)->update(['default_site'=> $id]);
            return 'changed';

        }else{

            return 'not change';
        }


    }
}
