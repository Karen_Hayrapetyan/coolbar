<?php

namespace App\Http\Controllers\Bars;

use App\Model\Custom_Pages;
use App\Model\Site;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Model\FacebookBarSettings;
use App\Model\Bar;

class FacebookBarController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');

        //share default site for auth user
        $this->middleware('site.sharing');

        // check if user confirmed email
        $this->middleware('verify.user');

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        dd(Auth::user()->default_site);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


        if(Auth::user()->default_site != 0 ){


            if(FacebookBarSettings::where('site_id',Auth::user()->default_site)->first()){
                Session::flash('msg','You Have Created This Bar You Can Edit It.');
                return redirect()->action('Bars\FacebookBarController@edit',Auth::user()->default_site);
            }
            $default = Site::where('id',Auth::user()->default_site)->first();
            return view('facebook_bar/facebook_create')->withdefault_site($default );
        }else{
            Session::flash('msg','You Have Not Created Sites Yet.');
            return redirect()->action('UserSitesController@create');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $data = $request->all();


        //add settings into facebook_bar_settings table
        FacebookBarSettings::create([
            'text' => substr($data['text'],0,60),
            'share' => !empty($data['share']) ? 'true' : 'false',
            'text_fb'=> !empty($data['text_fb']) ? 'standard' : 'button',
            'back_color'=> $data['back_color'],
            'text_color'=> $data['text_color'],
            'text_size'=> $data['text_size'],
            'bar_position' => $data['bar_position'],
            'site_id'=> Auth::user()->default_site,
            'user_id'=> Auth::user()->id,
            'duration'=>$data['duration']
        ]);



        if(!empty($data['pages'] )){

            $bar_id = Bar::where('bar_name','Facebook')->first()->id;
            //check and add custom pages into DB
            checkCustomUrl($data['pages'],$bar_id,'create');
        }

        Session::flash('msg','Your Configs  saved.');
        return redirect('/home');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['settings'] = FacebookBarSettings::where('site_id',$id)->where('user_id',Auth::user()->id)->first();
        $data['def_site'] = Site::where('id',$id)->where('user_id',Auth::user()->id)->first();
        $data['pages'] = Custom_Pages::where('site_id',$id)->get();
        return view('facebook_bar/edit',$data);

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();

        //update settings into facebook_bar_settings table

        FacebookBarSettings::where('site_id',Auth::user()->default_site)->where('user_id',Auth::user()->id)->update(array(
            'text' => substr($data['text'],0,60),
            'share' => !empty($data['share']) ? 'true' : 'false',
            'text_fb'=> !empty($data['text_fb']) ? 'standard' : 'button',
            'back_color'=> $data['back_color'],
            'text_color'=> $data['text_color'],
            'text_size'=> $data['text_size'],
            'bar_position' => $data['bar_position'],
            'site_id'=> Auth::user()->default_site,
            'user_id'=> Auth::user()->id,
            'duration'=>$data['duration']
        ));




        if(!empty($data['pages'] )){

            //delete all pages by site_id
            Custom_Pages::where('site_id',Auth::user()->default_site)->delete();

            $bar_id = Bar::where('bar_name','Facebook')->first()->id;
            //check and add custom pages into DB
            checkCustomUrl($data['pages'],$bar_id,'update');
        }

        Session::flash('msg','Your Configs  Edited.');
        return redirect('/home');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
