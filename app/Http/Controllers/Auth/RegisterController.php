<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */


    use RegistersUsers;
    /**
     * Override RegistersUsers register function
     *
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {

        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        return redirect('verifyEmailFirst');
//        $this->guard()->login($user);

        return $this->registered($request, $user)
            ?: redirect($this->redirectPath());
    }


    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('sendEmailDone');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',

        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {

        $user =  User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'verifyToken' =>Str::random(40),
        ]);

        //If User Created Sending Verify Email
        $thisUser = User::findOrFail($user->id);
        $this->sendEmail($thisUser);
    }

    /*
     *
     * Sending Verify Email After Register
     *
     */
    public function sendEmail($thisUser)
    {

        //Send email to registered email
        $status =  Mail::send('email.send', ['verifyToken' => $thisUser->verifyToken], function ($message) use($thisUser)
        {

            $message->from('hellobarkar@gmail.com', 'Cool Bar');

            $message->to($thisUser['email']);

        });

        //Checking if sent email
        if($status!=NULL){

            Session::flash('msg','Verify Message Not Sent');
            return redirect()->route('register');
        }

    }

    /*
     *
     * Sending verify message to view
     *
     * */

    public function verifyEmailFirst()
    {
        Session::flash('msg','Congrats! Please check your email to verify your account.');
        return redirect()->route('register');
    }


    /*
     *
     * Checking Confirmed VerifyToken
     *
     * */
    public function sendEmailDone($verifyToken,Request $request)
    {


        $user = User::where('verifyToken',$verifyToken)->first();

        if($user){

            User::where('verifyToken',$verifyToken)->update(['status'=>1,'verifyToken'=>NULL]);
            Auth::loginUsingId($user->id);
            return redirect('/home');


        }else{
            Session::flash('msg','Please Login ');
            return redirect('/login');
        }
    }


}
