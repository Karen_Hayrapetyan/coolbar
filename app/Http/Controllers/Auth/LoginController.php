<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        $this->middleware('guest')->except('logout');
    }




    /*
     *
     * Login By Google
     *
     * */
    public function redirectToProvider()
    {
        return Socialite::driver('google')->redirect();

    }

    /*
     * Google Login function
     *
     * */

    public function handleProviderCallback()
    {

        //geting user from google
        $userSocial = Socialite::driver('google')->user();

        //checking is user exists in our db
        $findUser = User::where('email',$userSocial->getEmail())->first();


        //if user exists just login , otherwise creating new user
        If($findUser){

            if($findUser->socialLogin == 1){
                auth()->login($findUser);
                return redirect('/home');
            }else {
                Session::flash('msg', 'The email has already been taken.');
                return redirect('/login');
            }
        }
        else{

            $user = new User();
            $id = $userSocial->getId();
            $user->name = $userSocial->getName();
            $user->status = 1;
            $user->socialLogin = 1;
            $user->email = $userSocial->getEmail();
            $user->password = md5($id);
            $user->save();
            Auth::login($user);
            return redirect('/home');
        }


    }

    public function all_log(Request $request){

        $this->guard()->logout();

        $request->session()->invalidate();
    }

}
