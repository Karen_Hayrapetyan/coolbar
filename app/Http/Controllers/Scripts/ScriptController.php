<?php

namespace App\Http\Controllers\Scripts;

use App\Model\Bar;
use App\Model\Custom_Pages;
use App\Model\FacebookBarSettings;
use App\Model\Site;
use App\Model\Visitor;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ScriptController extends Controller
{


    public function __construct()
    {
//        $this->middleware('auth');

        //share default site for auth user
        $this->middleware('site.sharing');

        // check if user confirmed email
//        $this->middleware('verify.user');

    }

    /*
     *
     * Getting Unique Script Link By User_Id
     *
     * */
    public function set_script($id){

        $site =Site::where('id', $id)->where('user_id',Auth::user()->id)->select('site_host')->first();
        $data['site'] = $site->site_host;
        $data['script'] = htmlspecialchars("<script src='".url('/')."/redirectScript/".md5($id).".js'  id='src-api'></script>");

        return view('sites/get_script',$data);
    }


    /**
     *
     * echo js script with existing settings
     *
     */

    public function loadBar($settings,$parent_url,$custom_text){
        echo " function loadBar() {
              
                //Appending css link
                  
                    var csslink  = document.createElement('link');
                    var head = document.head;
                    csslink.rel  = 'stylesheet';
                    csslink.type = 'text/css';
                    csslink.href = 'http://coolbar-new.dev/css/fblktx.css';
                    csslink.media = 'all';
                    head.appendChild(csslink);

                // Appending empty div 
                    var parent = document.body;
                    var newChild = '<div class=\"likerow\" id=\"likerow\">'+
                        '<div class=\"fblktxx\">'+
                        '<div class=\"likeBtn\" id=\"likeBtn\">'+
                        'Like Us On FaceBook' +
                        '</div>'+
                        '</div>'+
                        '</div>';
                    parent.insertAdjacentHTML('beforeend', newChild);

                // FaceBook iframe
                    var fbIframe = '<iframe src=\"https://www.facebook.com/plugins/like.php?href=" . $parent_url . "&width=450&layout=" . $settings->text_fb . "&action=like&size=small&show_faces=true&share=" . $settings->share . "&height=80&appId\"' +
                        ' width=\"450\" ' +
                        'height=\"80\" ' +
                        'style=\"border:none;overflow:hidden\"' +
                        ' scrolling=\"no\" frameborder=\"0\"' +
                        ' allowTransparency=\"true\">' +
                        '</iframe>';
                        
                // Set styles by Database Settings
                        document.getElementById(\"likeBtn\").innerHTML ='<div  class=\"likeBtn likeBtn2\" id=\"\">" . $custom_text . "</div>'+fbIframe ;
                        
                        document.getElementById(\"likeBtn\").style.color = \".$settings->text_color.\";
                        document.getElementById(\"likerow\").style.backgroundColor = \"" . $settings->back_color . "\";
                        document.getElementById(\"likeBtn\").style.fontSize = \"" . $settings->text_size . "\";
                       
                        var likerow = document.getElementById(\"likerow\")
                        
                        if('" . $settings->bar_position . "' == 'top'){
                        
                            likerow.classList.remove(\"positBottom\");
                            likerow.classList.add(\"positTop\");
                          
                        }else{
                             likerow.classList.add(\"positBottom\");
                            likerow.classList.remove(\"positTop\");
                        }
                        
                     
                        };
                        
                // Checking When will display FaceBookBar
                        if('" . $settings->duration . "'!= 'scroll'){
                        
                             window.onload = setTimeout(loadBar, " . $settings->duration . ");
                             
                        }else{
                        
                            var isScrolled = false;
                            
                            function scrolling() {
                                if(!isScrolled){
                                   loadBar()
                                }
                                isScrolled = true
                            }
                       
                            window.addEventListener(\"scroll\", scrolling);
                            
                           
                        
                    
                        };
                        
                                          
                        ";
        exit();
    }


    /**
     *
     * echo js by default settings
     *
     */
    public function loadBar_without_settings($parent_url){

        //   append bar with default settings
        echo " window.onload = function() {
                    var csslink  = document.createElement('link');
                    var head = document.head;
                    csslink.rel  = 'stylesheet';
                    csslink.type = 'text/css';
                    csslink.href = 'http://coolbar-new.dev/css/fblktx.css';
                    csslink.media = 'all';
                    head.appendChild(csslink);

                    var parent = document.body;
                    var newChild = '<div class=\"likerow\" id=\"likerow\">'+
                        '<div class=\"fblktxx\">'+
                        '<div class=\"likeBtn\" id=\"likeBtn\">'+
                        'Like Us On FaceBook' +
                        '</div>'+
                        '</div>'+
                        '</div>';
                    parent.insertAdjacentHTML('beforeend', newChild);

                       
                    var iframe = '<iframe src=\"https://www.facebook.com/plugins/like.php?href=" . $parent_url . "&width=450&layout=standard&action=like&size=small&show_faces=true&share=true&height=80&appId\"' +
                        ' width=\"450\" ' +
                        'height=\"80\" ' +
                        'style=\"border:none;overflow:hidden\"' +
                        ' scrolling=\"no\" frameborder=\"0\"' +
                        ' allowTransparency=\"true\">' +
                        '</iframe>';
                        
                        document.getElementById(\"likeBtn\").innerHTML ='<div  class=\"likeBtn likeBtn2\" id=\"\"></div>'+iframe ;                        
                        };
                        ";
    }


    /**
     *
     * add visitor by site_id and IP into visitors table
     *
     */

    public function visitors($site_id){

        //get client IP address
        $ip = ClientIp();
        $facebook_bar_id = Bar::where('bar_name','Facebook')->first()->id;


        //if have not created visitor_count by this client_ip create otherwise +1 visits count

        if(Visitor::where('site_id',$site_id)->where('client_ip',$ip)->first()){

            Visitor::where('site_id',$site_id)->where('client_ip',$ip)->increment('visits_count');

        }else{

             Visitor::create([

                'site_id' => $site_id,
                'bar_id' =>$facebook_bar_id,
                'client_ip' =>$ip,
                'visits_count' =>  1,
            ]);
        }
        return true;
    }


    /**
     *
     * Set cookie fix time when was  show  bar
     *
     */

    public function set_cookie($bar_name){

        $cookie_name = $bar_name;
        $cookie_value = time();
        setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/"); // 86400 = 1 day
    }


    /**
     *
     * return script
     *
     */

    public function get_script($id,$parent_url){

        //add visitor by site_id and IP into visitors table
        $this->visitors($id);

        //set cookie when was show this bar
        $this->set_cookie('Facebook');

        //getting site settings
        $settings = FacebookBarSettings::where('site_id', $id)->first();

        //check if site have settings change default settings by existing
        if ($settings != NULL) {

            $custom_text = '';
            if ($settings->text_fb != 'standard')
                $custom_text = $settings->text;

            $this->loadBar($settings,$parent_url,$custom_text);

        } else {

            $this->loadBar_without_settings($parent_url);
        }
    }


    /**
     *
     * handling Request and return js code
     *
     */
    public function generate_script()
    {

        //get parent Url
        $parent_url = $_SERVER["HTTP_REFERER"];


        // explode parent Url in path,host,scheme
        $url = parse_url($parent_url);

        //get filename and find it in DB
        $file_name = $_GET['file'];
        $md5 = substr($file_name, 0, -3);
        $site = DB::select('Select * from sites where MD5(id) = "' . $md5 . '"');



        //check if request Url and Url in Db is same
        if(isset($site )){

            //check if request Url have path

            if(isset($url['path'])){

                if(Custom_Pages::where('site_id', $site[0]->id)->where('page','/*')->first()){

                    $this->get_script($site[0]->id,$parent_url);
                }

                //getting custom pages by site_id
                $custom_page = Custom_Pages::where('site_id', $site[0]->id)->where('page',$url['path'])->first();

                //check if request Url_path and Custom page existing
                if(isset($custom_page) || $url['path'] == '/' ){

                    $this->get_script($site[0]->id,$parent_url);
                }else{
                    //check in Db page by page if exist /*
                    $pages = explode("/",$url['path']);
                    array_shift($pages);
                    $path = "";

                    foreach ($pages as $page){

                        $path = $path."/".$page;
                        if(Custom_Pages::where('site_id', $site[0]->id)->where('page',$path."/*")->first()){

                            $this->get_script($site[0]->id,$parent_url);
                        }
                    }
                }

            }else{

                $this->get_script($site[0]->id,$parent_url);
            };
        };


    }
}






