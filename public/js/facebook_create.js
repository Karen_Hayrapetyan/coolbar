/**
 * Created by user on 11/10/17.
 */

$("#selectSite").change(function() {

    var id = $(this).val();
    $.ajax({
        url: 'http://coolbar-new.dev/sites/change_default/'+id,
        type: 'get',
        data: { },
        success: function(response)
        {
            location.reload();
            console.log(response);
        }
    });
});




$(function() {
    $('#cp2').colorpicker();
    $('#cp3').colorpicker();
});

$("#ex8").slider({
    tooltip: 'always'
});
$(".sizeSlider").on('change', function(){
    var slide = $("#ex8").val();
    $(document).find(".likeBtn").css("font-size", slide+'px');
});

$("#background").change(function(){
    $(".likerow").css('background', $(this).val());
    $(".likeBtn").css('background', $(this).val());
});

$("#textcolor").change(function(){

    $(".likeBtn").css('color', $(this).val());
});

$("#text").change(function(){

    if($(this).val().length < 60){
        $(".text2").html($(this).val());
    }else {
        $(".text2").html($(this).val().substring(0, 60));
    }

});

$("#radio5").change(function(){

    $(".likerow").removeClass("positBottom");
    $(".likerow").addClass("positTop");
});

$("#radio6").change(function() {
    $(".likerow").removeClass("positTop");
    $(".likerow").addClass("positBottom");
});

$('#radioBtn a').on('click', function(){
    var sel = $(this).data('title');
    var tog = $(this).data('toggle');
    $('#'+tog).prop('value', sel);

    $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
    $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
});

$('#radioBtn #mobileOff').on('click', function(){

    $(".settings2").css('display','none');
    $(".settings3").css('display','block');
});

$('#radioBtn #mobileOn').on('click', function(){

    $(".settings2").css('display','block');
    $(".settings3").css('display','none');
});

function share() {
    var shareBtn = document.getElementById('share');
    if(shareBtn.checked){
        $(".shareLogo").css('display','inline');
    }else {
        $(".shareLogo").css('display','none');
    }
}


function text_fb() {

    var text_fb = document.getElementById('text_fb');

    if(text_fb.checked){

        $(".text2").css('display','none');
        $(".size").css('display','none');
        $(".textInput").css('display','none');
        $(".text_fb").css('display','inline');

    } else {

        $(".text2").css('display','inline');
        $(".textInput").css('display','block');
        $(".size").css('display','block');
        $(".text_fb").css('display','none');

    }
}

function position() {
    var bottom = document.getElementById('radio6');
    if(bottom.checked){
        $(".likerow").removeClass("positTop");
        $(".likerow").addClass("positBottom");
    }
}



    // share();
    $("#share").change(function(){
        share();
    });

    $("#text_fb").change(function(){
        text_fb();
    });

window.addEventListener('load',
    function() {
        share();
        text_fb();
        position();
    }, false);


//add input for custom pages

//set a counter
var i = $('#form-step2 :input').length + 1;

//add input
$('a#add_btn').click(function () {
    $('<p><input type="text" name="pages[]" id="' + i + '" value="/" class="add_page" />' +
        '<a class="dynamic-link" href="#step2">close</a></p>').fadeIn("slow").appendTo('#extender');
    i++;
    return false;
});


//fadeout selected item and remove
$("#form-step2").on('click', '.dynamic-link', function () {

    $(this).parent().fadeOut(300, function () {
        $(this).empty();
        return false;
    });
});
