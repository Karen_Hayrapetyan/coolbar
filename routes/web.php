<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::prefix('sites')->group(function () {

    Route::get('/', 'UserSitesController@index')->name('mySites');
    Route::get('create', 'UserSitesController@create')->name('create');
    Route::post('save', 'UserSitesController@save')->name('save');
    Route::get('edit/{id}', 'UserSitesController@edit')->name('edit');
    Route::get('delete/{id}', 'UserSitesController@delete')->name('delete');
    Route::get('update/{id}', 'UserSitesController@update')->name('update');
    Route::get('change_default/{id}', 'UserSitesController@change_default')->name('change_default');

});

Route::prefix('scripts')->group(function (){

    Route::get('set_script/{id}', 'Scripts\ScriptController@set_script')->name('set_script');
    Route::any('/generate_script', 'Scripts\ScriptController@generate_script');

});



//Bars Crud
Route::resource('bars', 'Bars\BarController');

//FacebookBar Crud
Route::resource('facebook', 'Bars\FacebookBarController');


//QuizBar Crud
Route::resource('quiz_bar', 'Bars\QuizController');

Route::prefix('facebook')->group(function (){

    Route::get('ajax_delete_page/{id}', 'Bars\FacebookBarController@ajax_delete_page')->name('ajax_delete_page');

});

//errors
Route::prefix('error')->group(function (){
    Route::get('not-valid', 'ErrorController@notValid')->name('not-valid');
});

//google login

Route::get('login/google', 'Auth\LoginController@redirectToProvider')->name('login/google');
Route::get('login/google/callback', 'Auth\LoginController@handleProviderCallback');

//verify email
Route::get('verifyEmailFirst', 'Auth\RegisterController@verifyEmailFirst')->name('verifyEmailFirst');
Route::get('verify/{verifyToken}', 'Auth\RegisterController@sendEmailDone')->name('sendEmailDone');