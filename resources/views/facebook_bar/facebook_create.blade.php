@extends('layouts.app')
@section('base_nav')
    <select class="form-control default_site_small" id="selectSite" name="bar" style="border: 2px solid gray;display: inline;margin-top: 6px;width: 250px;margin-right: 50px;">>
        @forelse($sites as $site)
            <option value="{{$site->id}}" aria-checked="true"
                    @if($site->id == Auth::user()->default_site)
                    selected
                    @endif>
                {{$site->site_host}}
            </option>
        @empty
            <option value="">You Have Not Created Sites Yet</option>
        @endforelse
    </select>
    <a class="navbar-brand" href="{{action('HomeController@index')}}" style="margin-left: 50px;float: left">CoolBar</a>
    @endsection
@section('content')
    <div class="navbar-default sidebar" role="navigation">
        <div class="left_bar" style="margin-left: 20px;width: 350px;float: left">
            <form class="form-horizontal" id="contactForm" method="post" role="form" enctype="multipart/form-data" action="{{url('/facebook')}}" style="">
                {{ csrf_field() }}

                <div class="selects">
                    <div class="">
                        <div class="panel panel-default">
                            <!-- Default panel contents -->
                            <div class="panel-heading">FaceBookBar Settings</div>

                            <!-- List group -->
                            <ul class="list-group">
                                <li class="list-group-item">
                                    Cool Bar BrandLogo
                                    <div class="material-switch pull-right">
                                        <input id="someSwitchOptionDefault" name="brand_logo" type="checkbox" checked/>
                                        <label for="someSwitchOptionDefault" class="label-default"></label>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    Animation
                                    <div class="material-switch pull-right">
                                        <input id="someSwitchOptionPrimary" name="animation" type="checkbox" checked/>
                                        <label for="someSwitchOptionPrimary" class="label-primary"></label>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    Share Button
                                    <div class=" pull-right">
                                        <label for="share" class="label-success"></label>
                                        <input id="share" name="share" type="checkbox" checked/>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    FaceBook Text
                                    <div class=" pull-right">
                                        <label for="text_fb" class="label-success"></label>
                                        <input id="text_fb" name="text_fb" type="checkbox" checked/>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <h5>Background Color</h5>
                        <div class="colpiker">
                            <div id="cp2" class="input-group colorpicker-component">
                                <input type="text" value="#00AABB" class="form-control" name="back_color" id="background" />
                                <span class="input-group-addon"><i></i></span>
                            </div>
                        </div>
                        <h5>Text Color</h5>
                        <div class="colpiker2">
                            <div id="cp3" class="input-group colorpicker-component">
                                <input type="text" value="#3c299d" class="form-control" name="text_color" id="textcolor" />
                                <span class="input-group-addon"><i></i></span>
                            </div>
                        </div>
                        <div class="size" style="display: none">
                            <h5>Text Size</h5>
                            <input id="ex8" class="sizeSlider" name="text_size" data-slider-id='ex1Slider' type="text" data-slider-min="0" data-slider-max="24" data-slider-step="1" data-slider-value="12"/>
                        </div>

                        <div class="textInput" style="display: none">
                            <h5>Change Text</h5>
                            <input id="text" class="" name="text" type="text" value="Like Us On FaceBook!!!"/>
                        </div>

                        <br>
                        <div class="textInput2 col-md-11 row">
                            <h5>Change Url Page Where does it display?</h5>
                            <h6 style="color: red">Note:/* will display in all pages</h6>
                            <input id="customUrl" class="col-md-12" name="customUrl" type="text" value="{{$default_site['site_host']}}" disabled="disabled"/>
                            <div  id="form-step2" class="form-vertical">
                                    <div id="extender"></div>
                                <br>
                                    <p><a id="add_btn" class="btn btn-primary">Add</a></p>
                            </div>
                        </div>
                        <br>
                        <br>
                        <br><br>
                        <div class="form-group col-md-10">
                            <label for="sel1">When does it display?:</label>
                            <select name="duration" class="form-control" id="sel1">
                                <option value="0">Immediately</option>
                                <option value="5000">5 second delay</option>
                                <option value="20000">20 second delay</option>
                                <option value="60000">60 second delay</option>
                                <option value="scroll">After scrolling a little</option>
                            </select>
                        </div>
                        <br>
                        <div class="form-group">
                            <label for="mobile" class="col-sm-4 col-md-4 control-label text-right">Mobile Version</label>
                            <div class="col-sm-7 col-md-7">
                                <div class="input-group">
                                    <div id="radioBtn" class="btn-group">
                                        <a class="btn btn-primary btn-sm active" id="mobileOn" aria-checked="true">On</a>
                                        <a class="btn btn-primary btn-sm notActive" id="mobileOff">Off</a>
                                    </div>
                                    <input type="hidden" name="mobile" id="happy">
                                </div>
                            </div>
                        </div>

                        <div class="funkyradio" style="">
                            <div class="panel-heading">Message Position</div>
                            <div class="funkyradio-warning">
                                <input type="radio" class="positTop" name="bar_position" id="radio5" value="top" checked/>
                                <label for="radio5">Top</label>
                            </div>
                            <div class="funkyradio-info">
                                <input type="radio" class="positBottom" name="bar_position" id="radio6" value="bottom" />
                                <label for="radio6">Bottom</label>
                            </div>
                        </div>
                    </div>
                </div>


                <button type="submit" class="btn btn-danger">Save And Publish</button>
            </form>
        </div>
    </div>

    <div class="col-md-8 settings2">
        <div class="col-md-4 phone">
            <div class="device-container">
                <div class="device-mockup iphone6_plus portrait white">
                    <div class="device">
                        <div class="screen">
                            <div class="likerow" id="likerow" style="">
                                <div class="fblktxx">
                                    <div class="likeBtn" id="likeBtn">
                                        <img src="{{asset('images/LIKE2.png')}}" alt="likelogo"  class="likeLogo logo">
                                        <img src="{{asset('images/s_fb_button.png')}}" alt="sharelogo"  class="shareLogo logo">
                                        <span class="text2" style="display: none">Like Us On FaceBook!! </span>
                                        <span class="text_fb" style="font-size: 12px">  like this. Be the first of your friends.</span>
                                    </div>
                                </div>
                            </div>
                            <div class="siteShot" style="background-image: url('https://s.wordpress.com/mshots/v1/{{$default_site['full_url'] or 'https://www.google.am/'}}')">

                            </div>

                            <img src="" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-8 settings3">
        <div class="siteShot" style="background-image: url('https://s.wordpress.com/mshots/v1/{{$default_site['full_url'] or 'https://www.google.am/'}}')">
            <div class="likerow" id="likerow" style="">
                <div class="fblktxx">
                    <div class="likeBtn" id="likeBtn">
                        <img src="{{asset('images/LIKE2.png')}}" alt="likelogo"  class="likeLogo logo">
                        <img src="{{asset('images/s_fb_button.png')}}" alt="sharelogo"  class="shareLogo logo">
                        <span class="text2" style="display: none;">Like Us On FaceBook!! </span>
                        <span class="text_fb" style="font-size: 12px">  like this. Be the first of your friends.</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection
<script>

</script>
