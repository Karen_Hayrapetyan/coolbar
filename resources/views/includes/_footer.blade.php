<!-- jQuery -->
<script src="{{asset('tamplate/vendor/jquery/jquery.min.js')}}"></script>

<!-- Bootstrap Core JavaScript -->
<script src="{{asset('tamplate/vendor/bootstrap/js/bootstrap.min.js')}}"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="{{asset('tamplate/vendor/metisMenu/metisMenu.min.js')}}"></script>

<!-- Morris Charts JavaScript -->
<script src="{{asset('tamplate/vendor/raphael/raphael.min.js')}}"></script>

<!-- Custom Theme JavaScript -->
<script src="{{asset('tamplate/dist/js/sb-admin-2.js')}}"></script>

{{--Main Js--}}
<script src="{{asset('js/main.js')}}"></script>



