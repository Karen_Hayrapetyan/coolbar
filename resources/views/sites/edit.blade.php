@extends('layouts.main')

@section('defaultSite')
@endsection
@section('content')

    <div class="row addSite" style="margin: 90px">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Add Sites!!</div>

                <div class="panel-body">
                    @if(Session::has('msg'))
                        <div class="alert alert-info">
                            <a class="close" data-dismiss="alert">×</a>
                            {!!Session::get('msg')!!}
                        </div>
                    @endif
                    <span style="color: red">

                          @if($errors->any())
                            <h4>{{$errors->first()}}</h4>
                        @endif
                     </span>
                    <form method="get" action="{{action('UserSitesController@update',$site['id'])}}">
                        {{csrf_field()}}
                        {{method_field('PUT')}}
                        <input type="text" name="full_url" id="getUrl" class="form-control" value="{{$site['full_url']}}" placeholder="Your website URL">
                        <br>
                        <select name="user_timezone" id="timezone" class="form-control">
                            @foreach ($timezoneList as $timezone)
                                <option value="{{ $timezone }}">{{ $timezone }}</option>
                            @endforeach
                        </select>
                        <br>
                        <button type="submit" class="btn-default">Edit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

