@extends('layouts.main')

@section('defaultSite')
@endsection
@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">My Sites</h1>
        </div>
        <!-- /.col-lg-12 -->

    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-4 col-md-6">
            @if(Session::has('msg'))
                <div class="alert alert-info">
                    <a class="close" data-dismiss="alert">×</a>
                    {!!Session::get('msg')!!}
                </div>
            @endif
            <div class="" style="color: #0c0c0c;font-size: 18px;font-weight: 700">
                @if( empty($sites))
                    <p>You Have Not Created Sites Yet</p>
                @endif
                    <fieldset>


                    </fieldset>
                @foreach($sites as $site)

                    <div class="setSite{{$site->id}}" style="">
                        <span> {{$site->site_host}}</span>
                        <div id="{{$site->id}}">
                            <a href="{{action('UserSitesController@edit',$site->id)}}" class="btn btn-primary">
                                edit
                            </a>
                            <a href="{{action('UserSitesController@delete',$site->id)}}" class="btn btn-danger"  >
                                delete
                            </a>
                            <a href="{{action('Scripts\ScriptController@set_script',$site->id)}}" class="btn btn-danger">
                                Get script
                            </a>

                            <br/>
                        </div>
                        <br>
                    </div>
                @endforeach

            </div>

            </div>
        </div>

        </div>
    </div>

    <!-- /.row -->
    </div>
@endsection


