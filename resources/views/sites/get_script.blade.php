@extends('layouts.main')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Script</h1>
        </div>
        <!-- /.col-lg-12 -->

    </div>
    <!-- /.row -->
    <div class="row">
        <div class="">
            @if(Session::has('msg'))
                <div class="alert alert-info">
                    <a class="close" data-dismiss="alert">×</a>
                    {!!Session::get('msg')!!}
                </div>
            @endif
            <div class="" style="color: #0c0c0c;font-size: 18px;font-weight: 700">

                    <h4>Script code for site:</h4>
                    <p class="getScriptSite">{{$site}}</p>
                    <br>
                    <h4>Copy Script Code and Put it in Your Site Code</h4>
                    <div class="getScriptCode">
                        {{$script}}
                    </div>

            </div>

            </div>
        </div>

        </div>
    </div>

    <!-- /.row -->
    </div>
@endsection


