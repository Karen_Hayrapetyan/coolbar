@include('includes/_header')

<div id="wrapper">

    <!-- Navigation -->
    @section('navbar')
        @include('includes._navbar')
    @show

    @section('left_bar')
        @include('includes._leftBar')
    @show
    <div id="page-wrapper">

        @yield('content')
    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

    @section('footer')
        @include('includes/_footer')
    @show

